const Header = require('./common/header.component');
const sideMenu = require('./common/sidemenu.components');

const AddDoctorModal = require('./doctors/add-doctor.component');
const DoctorListHeader = require('./doctors/list-header.components');
const SpecialistCard = require('./doctors/specialist-card.component');


module.exports = {
    Header,
    sideMenu,
    AddDoctorModal,
    DoctorListHeader,
    SpecialistCard,
}