

const { pages } = require('./../po'); 

describe('Doctors page', () => {
    beforeEach(async () => {
        await pages('dashboard').open();
    });

it('Check page title', async () => {
await expect(browser).toHaveTitle('Appointment Planner - Syncfusion Angular Components Showcase App')
});


it('Open window for adding a new doctor', async () => {
    //click on doctors item in the side menu
    await pages('dashboard').sideMenu.item('doctors').click();
    //click on add new doctor btn
   await pages('doctors').doctorListHeader.addNewDoctorBtn.click();
    //check that modal window displayed
    await expect(pages('doctors').addDoctorModal.rootEl).toBeDisplayed();
});


it('Add a new doctor', async () => {
    //click on doctors item in the side menu
    await pages('dashboard').sideMenu.item('doctors').click();
    //click on add new doctor btn
    await pages('doctors').doctorListHeader.addNewDoctorBtn.click();
    //wait for visibility of modal window
   await pages('doctors').addDoctorModal.rootEl.waitForDisplayed();
    //fill all required fields

await pages('doctors').addDoctorModal.input('name').setValue('Uladzimir Ratskevich');
await pages('doctors').addDoctorModal.input('phone').setValue('1234567890');
await pages('doctors').addDoctorModal.input('email').setValue('doctoremail123@gmail.com');
await pages('doctors').addDoctorModal.input('education').setValue('medical university');
await pages('doctors').addDoctorModal.input('designation').setValue('test123');

await pages('doctors').addDoctorModal.saveBtn.click(); 

await expect(pages('doctors').addDoctorModal.rootEl).not.toBeDisplayed();


await expect(pages('doctors').specialistCard(8).name).toHaveText('Dr. Uladzimir Ratskevich');
await expect(pages('doctors').specialistCard(8).education).toHaveText('medical university', {ignoreCase: true});

});


it('Close a modal window for adding a new doctor', async () => {
    await pages('dashboard').sideMenu.item('doctors').click();
    await pages('doctors').doctorListHeader.addNewDoctorBtn.click();
    await pages('doctors').addDoctorModal.rootEl.waitForDisplayed();
    await pages('doctors').addDoctorModal.closeBtn.click();
    await expect(pages('doctors').addDoctorModal.rootEl).not.toBeDisplayed();
})

})