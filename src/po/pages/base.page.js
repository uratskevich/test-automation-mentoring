const { Header, sideMenu} = require('./../components');


class BasePage {

constructor(url) {
this.url = url;
this.header = new Header();
this.sideMenu = new sideMenu()
}

open() {
    return browser.url(this.url);
}
}

module.exports = BasePage;