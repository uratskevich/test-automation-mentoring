describe("Onliner test", () => {

    it("First test", async () => {
        await browser.url("https://www.onliner.by/");
       const pageTitle = await browser.getTitle();

      // console.log(pageTitle);
       expect(pageTitle).toEqual("Onlíner");
    });

    it('Click to search field', async () => {
        const searchField = await $('[class="fast-search__input"]');
        await searchField.click();
    });

    it('Set value', async () => {
        const input = await $('[class="fast-search__input"]')
        await input.setValue('Samsung');
    })

    it('Close search window', async () => {

        const searchButton = await $('.search__close:before');
        await searchButton.click();

    })
})